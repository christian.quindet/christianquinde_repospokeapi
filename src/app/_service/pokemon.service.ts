import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiPaths } from '../util/endpoints';
import { Page } from '../_domain/page';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {

  private apiUrl = ApiPaths.apipokemon;

  constructor(private http: HttpClient) {}

/* recupera los pokemones paginados y agrega el detalle de cada uno a la lista recuperada */
  getAllPokemonsPaginado(inicio:number,numRegs:number) {
    return this.http
      .get<any>(this.apiUrl + '?offset='+inicio+'&limit='+numRegs)
      .toPromise()
      .then((res) => <Page<any>>res)
      .then((data) => {
        var pokeStats: any = [];
        data.results.forEach((element) => {
          this.getDetallePokemon(element['url']).then((data) => {
            pokeStats.push({
              name: element['name'],
              url: element['url'],
              detalle: data,
            });
          });
        });
        data.results = pokeStats;
        return data;
      });
  }

  /* recupera el pokemon por nombre ingresado por pantalla */
  getPokemonesbyName(nombre: string) {
    return this.http
      .get<any>(this.apiUrl + '/' + nombre)
      .toPromise()
      .then((res) => <Object>res)
      .then((data) => {
        return data;
      });
  }

  /* recupera el detalle del pokemonn que se recupera */
  public getDetallePokemon(pokeuri: string) {
    return this.http
      .get<any>(pokeuri)
      .toPromise()
      .then((res) => <any>res)
      .then((data) => {
        return data;
      });
  }
}
