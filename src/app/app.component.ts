import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  /* se mantiene para pruebas unitarias */
  title = 'christianquinde_repospokeapi';

  constructor() {}
  ngOnInit(): void {}
  
}
