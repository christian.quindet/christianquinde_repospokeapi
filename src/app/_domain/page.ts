/* estructura base para la respuesta que devuelve el api pokemon */
export interface Page<T> {
  results: T[];
  count:number;
  next:string;
  previous:string;
}
