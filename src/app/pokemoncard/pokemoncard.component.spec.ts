import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PokemoncardComponent } from './pokemoncard.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

describe('PokemoncardComponent', () => {
  let component: PokemoncardComponent;
  let fixture: ComponentFixture<PokemoncardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemoncardComponent ],
      imports: [ HttpClientTestingModule ,FormsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PokemoncardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Deberia crearse', () => {
    expect(component).toBeTruthy();
  });

  it('Debe cargar datos al iniciar la primera carga', () => {
    component.recuperaPokemones(0,50);
    expect(component.listPokemons).toHaveSize;
  });

  it('Debe consultar valores de paginado', () => {
    component.numeroPag=5;
    component.siguientePag();
    expect(component.listPokemons).toHaveSize;
  });

  it('Consulta pokemon por nombre', () => {
    component.searchPokemon("pikachu");
    expect(component.listPokemons).toHaveSize;
  });
  
  it('Se dar click en boton de busqueda y rellena la variable pokemon detalles', () => {
    component.nombreAbuscar='charmander'
    const btnElement=fixture.debugElement.query(By.css('#botonBuscador'));
    btnElement.nativeElement.click()
    expect(component.listPokemons).toHaveSize;

  });

});
