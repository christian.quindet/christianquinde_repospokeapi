import { Component, OnInit, Input } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { PokemonService } from "../_service/pokemon.service";

@Component({
  selector: "app-pokemoncard",
  templateUrl: "./pokemoncard.component.html",
  styleUrls: ["./pokemoncard.component.css"],
})
export class PokemoncardComponent implements OnInit {
  /* variables para control de paginado, busqueda */
  numeroPag: number;
  regsPorPag: number;
  totalRegs: number;
  totalPags: number;
  constructor(private pokemonService: PokemonService) {
    /* por defecto se establece los siquieres valores ya que no van a cambiar */
    this.numeroPag = 0;
    this.regsPorPag = 4;
    this.totalRegs = 1281;
    this.totalPags = this.totalRegs / this.regsPorPag + 1;
  }
  /* inicializamos nuestra primera consulta de pokemones */
  ngOnInit() {
    this.recuperaPokemones(0, this.regsPorPag);
  }

  listPokemons: any = [];
  pokemonSeleccionado: any;
  nombreAbuscar: string = "";

  /* permite buscar el pokemons por su nombre */
  searchPokemon(nombrePokemon: string) {
    this.pokemonSeleccionado = null;
    this.listPokemons = [];
    this.numeroPag = 0;
    this.pokemonService
      .getPokemonesbyName(nombrePokemon)
      .then((data) => {
        if (data != null) {
          var pokeStats: any = [];
          pokeStats.push({
            name: nombrePokemon,
            url: "",
            detalle: data,
          });
          this.listPokemons = pokeStats;
        }
      })
      .catch((data) => {});
  }
/* metdo que permite detallar el pokemonn seleccionado */
  showMePokeDetalle(pokemon: any) {
    this.pokemonSeleccionado = pokemon;
  }
/* permite recuperar el paginado anterior de pokemonnes */
  anteriorPag() {
    if (this.numeroPag > 0) {
      this.numeroPag--;
      var inicio = this.numeroPag * this.regsPorPag;
      this.recuperaPokemones(inicio, this.regsPorPag);
    }
  }
/* permite recuperar el paginado siguiente de pokemonnes */
  siguientePag() {
    if (this.numeroPag >= 0 && this.numeroPag <= this.totalPags) {
      this.numeroPag++;
      var inicio = this.numeroPag * this.regsPorPag;
      this.recuperaPokemones(inicio, this.regsPorPag);
    }
  }
/* metodo que recupera los pokemones en base a los parametros  */
  recuperaPokemones(inicio: number, numRegs: number) {
    this.pokemonService.getAllPokemonsPaginado(inicio, numRegs).then((data) => {
      console.log(data);
      this.listPokemons = data.results;
    });
  }
}
